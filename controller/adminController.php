<?php

//TODO Verification Admin

if(!isset($_GET['action']))
{
    $action="";
}
else{
    $action=filter_var($_GET['action'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}

switch ($action){
    case "admin":
        if(isset($_SESSION['email']))
        {
            require('view/admin.php');
        }
        else{
            require('view/404.php');
        }
        
    break;
    case "traitementFormModif":
        if(isset($_POST['nom'])&& isset($_POST['prenom'])&& isset($_POST['email'])&&isset($_POST['id']))
        {
            require("./model/bdd.php");
            require("./model/managers/stagiaireManager.php");
            $prenom=filter_var($_POST['prenom'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $nom=filter_var($_POST['nom'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $email=filter_var($_POST['email'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $id=filter_var($_POST['id'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $lePDO=etablirCo();
            $stagiaireManager=new StagiaireManager($lePDO);
            $stagiaire=new Stagiaire();
            $stagiaire->setIdStagiaire($id);
            $stagiaire->setNom($nom);
            $stagiaire->setPrenom($prenom);
            $stagiaire->setEmail($email);
            $stagiaireManager->updateStagiaire($stagiaire);
        }
    break;
    case "formModif":
        require("model/bdd.php");
        require("model/managers/stagiaireManager.php");
        $idStagiaire=filter_var($_GET['num'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $lePDO=etablirCo();
        $stagiaireManager=new StagiaireManager($lePDO);
        $leStagiaire=$stagiaireManager->getStagiaireById($idStagiaire);
        var_dump($leStagiaire);
        require("view/modifStagiaire.php");
        
    break;

    default :
    require('view/home.php');
}



?>