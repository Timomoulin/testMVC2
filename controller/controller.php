<?php

if(!isset($_GET['action']))
{
    $action="";
}
else{
    $action=filter_var($_GET['action'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}

switch ($action){
    case "home":
        require('model/bdd.php');
        require('model/managers/stagiaireManager.php');
        //un objet qui represente la co a la BDD;
        $unPDO=etablirCo();
        //j'instancie un objet de la classe StagiaireManager;
        $stagiaireManager=new StagiaireManager($unPDO);
        //j'apelle la méthode getAllStagiaire depuis l'objet $stagiaireManager
        //et je recupere le resultat dans la variable $lesStagiaires
        $lesStagiaires=$stagiaireManager->getAllStagiaire();
        
        require('view/home.php');
    break;
        case "stagiaire":
            require('model/bdd.php');
            require('model/managers/stagiaireManager.php');
            require('model/managers/evaluationManager.php');
            require_once("model/managers/matiereManager.php");

            //je recup dans l'url la valeur du parametre num
            $idStagiaire=filter_var($_GET['num'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $unPDO=etablirCo();
            $matiereManager=new MatiereManager($unPDO);
            $stagiaireManager=new StagiaireManager($unPDO);
            $leStagiaire=$stagiaireManager->getStagiaireById($idStagiaire);
            $evaluationManager=new EvaluationManager($unPDO);
            $lesEvaluations=$evaluationManager->getEvaluationByIdStagiaire($idStagiaire);
           
            require('view/stagiaire.php');
            break;

    case "contact" :
        require('view/contact.php');
    break;

    case"login":
        if(isset($_SESSION["email"]))
        {
            session_unset();
            session_destroy();
            header("location:index.php?path=main&action=login");
        }
        else{
        require('view/login.php');
        }
        break;
    case"handleCo":
        require("model/bdd.php");
        require("model/managers/stagiaireManager.php");
        $email=filter_var($_POST['email'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp=filter_var($_POST['mdp'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
       $mdp=hash('sha256',$mdp);
  
        $lePDO=etablirCo();
       $stagiaireManager=new StagiaireManager($lePDO);
       $stagiaire=$stagiaireManager->getStagiaireByEmailAndPassword($email,$mdp);
       var_dump($stagiaire);
        if($stagiaire==false)
        {
            $_SESSION['error']="Tentative de connexion échouer";
            header("location:index.php?path=main&action=login");
            die;
        }
        else{
            session_unset();
            $_SESSION["email"]=$stagiaire->getEmail();
            $_SESSION["id"]=$stagiaire->getIdStagiaire();
            $_SESSION['role']="admin";
            header("location:index.php");
        }
        break;
    case"formInscription":
        require('view/register.php');
        break;
    case"traitementInscription":
        var_dump($_POST);
        $prenom=filter_var($_POST['prenom'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $nom=filter_var($_POST['nom'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $email=filter_var($_POST['email'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp1=filter_var($_POST['mdp1'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp2=filter_var($_POST['mdp2'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        require('model/bdd.php');
        addStagiaire($nom,$prenom,$email);
        header("location:index.php");
        
        //@TODO Verification des données

        break;
    default :
    require('view/home.php');
}



?>