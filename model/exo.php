<?php
require("Joueur.php");
require("Rencontre.php");
require("Equipe.php");
$joueur1=new Joueur("Lloris","Hugo","gardien",1,"gaucher");
$joueur2=new Joueur("Varane","Raphael","defenseur",5,"droitier");
$joueur3=new Joueur("Nsimba","Louis","attaquant",9,"ambidextre");

$equipe1=new Equipe();
$equipe1->setTitulaires([$joueur1,$joueur2,$joueur3]);
$equipe1->setFormation("1-1-1");

$equipe2=new Equipe();
$equipe2->setTitulaires($joueur3,$joueur3,$joueur3);
$equipe2->setFormation("1³");

$match1=new Rencontre();
$match1->setEquipe1($equipe1);
$match1->setEquipe2($equipe2);

var_dump($match1);

echo $match1->getEquipe1()->getTitulaires()[0]->getPrenom();
echo("<pre>");
print_r($match1->getEquipe1()->getTitulaires());
echo("</pre>");
?>