<?php
require_once("./model/classes/matiere.class.php");
class MatiereManager {
    private $lePDO;
    
    public function __construct($unPDO)
    {
        $this->lePDO=$unPDO;
    }


  
    public function getMatiereById($id)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM matiere WHERE idMatiere=:uneId ");
            $sql->bindParam(":uneId",$id);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Matiere");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    /**
     * Get the value of lePDO
     */ 
    public function getLePDO()
    {
        return $this->lePDO;
    }

    /**
     * Set the value of lePDO
     *
     * @return  self
     */ 
    public function setLePDO($lePDO)
    {
        $this->lePDO = $lePDO;

        return $this;
    }
}
?>