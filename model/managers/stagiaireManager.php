<?php
  require("./model/classes/stagiaire.class.php");
class StagiaireManager{
    private $lePDO;
    
    /**
     * Le constructeur de la classe StagiaireManager 
     * qui permet de créer un objet grace a un objet de classe PDO 
     * fournit en argument
     * @Param un objet de type PDO qui represente la co a la BDD
     */
    public function __construct($unPDO)
    {
        $this->lePDO=$unPDO;
    }

    /**
     * Get the value of lePDO
     */ 
    public function getLePDO()
    {
        return $this->lePDO;
    }

    /**
     * Set the value of lePDO
     *
     * @return  self
     */ 
    public function setLePDO($lePDO)
    {
        $this->lePDO = $lePDO;

        return $this;
    }

    /**
     * Une methode qui permet d'extraire tous les stagiaires de la table stagiaire
     * de notre BDD
     * @Return Un array d'objets de la classe Stagiaire 
     */
    public function getAllStagiaire(){

        try {
          
            $bdd=$this->lePDO;
            $sql =$bdd->prepare("SELECT * FROM stagiaire ORDER BY idStagiaire");
            $sql->execute();
            $resultat = ($sql->fetchAll(PDO::FETCH_CLASS,"Stagiaire"));
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function getStagiaireById($id)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM stagiaire WHERE idStagiaire=:uneId ");
            $sql->bindParam(":uneId",$id);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Stagiaire");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function updateStagiaire(Stagiaire $unStagiaire)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("UPDATE stagiaire set nom=:nom, prenom=:prenom, email=:email where idStagiaire=:id ");
          
            $sql->bindValue(":nom",$unStagiaire->getNom());
            $sql->bindValue(":prenom",$unStagiaire->getPrenom());
            $sql->bindValue(":email",$unStagiaire->getEmail());
            $sql->bindValue(":id",$unStagiaire->getIdStagiaire());
            $sql->execute();
          

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function addStagiaire(Stagiaire $unStagiaire)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("INSERT INTO stagiaire (nom,prenom,email,mdp) values(:nom,:prenom,:email,:mdp) ");
            $sql->bindValue(":nom",$unStagiaire->getNom());
            $sql->bindValue(":prenom",$unStagiaire->getPrenom());
            $sql->bindValue(":email",$unStagiaire->getEmail());
            $sql->bindValue(":mdp",$unStagiaire->getMdp());
            $sql->execute();
          

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
    public function getStagiaireByEmailAndPassword($email,$mdp)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare(" SELECT * FROM stagiaire where email=:email and mdp=:mdp");
            $sql->bindParam(":email",$email);
            $sql->bindParam(":mdp",$mdp);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Stagiaire");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

}
?>