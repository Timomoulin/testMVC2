<?php
 require("./model/classes/evaluation.class.php");
class EvaluationManager{
    private $lePDO;

    public function __construct($unPDO)
    {
        $this->lePDO=$unPDO;
    }

    public function getEvaluationByIdStagiaire($idStagiaire)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM evaluation WHERE idStagiaire=:idStagiaire ORDER BY dateEvaluation ");
            $sql->bindParam(":idStagiaire",$idStagiaire);
            $sql->execute();
            $resultat = ($sql->fetchAll(PDO::FETCH_CLASS,"Evaluation"));
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    /**
     * Get the value of lePDO
     */ 
    public function getLePDO()
    {
        return $this->lePDO;
    }

    /**
     * Set the value of lePDO
     *
     * @return  self
     */ 
    public function setLePDO($lePDO)
    {
        $this->lePDO = $lePDO;

        return $this;
    }
}
?>