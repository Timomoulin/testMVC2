<?php
class Joueur{
private $nom;
private $prenom;
private $poste;
private $num;
private $lateralite;


public function __construct($unNom,$unPrenom,$unPoste,$unNum,$uneLateralite)
{
    $this->nom=$unNom;
    $this->prenom=$unPrenom;
    $this->poste=$unPoste;
    $this->num=$unNum;
    $this->lateralite=$uneLateralite;
}

/**
 * Get the value of nom
 */ 
public function getNom()
{
return $this->nom;
}

/**
 * Set the value of nom
 *
 * @return  self
 */ 
public function setNom($nom)
{
$this->nom = $nom;

return $this;
}

/**
 * Get the value of prenom
 */ 
public function getPrenom()
{
return $this->prenom;
}

/**
 * Set the value of prenom
 *
 * @return  self
 */ 
public function setPrenom($prenom)
{
$this->prenom = $prenom;

return $this;
}

/**
 * Get the value of poste
 */ 
public function getPoste()
{
return $this->poste;
}

/**
 * Set the value of poste
 *
 * @return  self
 */ 
public function setPoste($poste)
{
$this->poste = $poste;

return $this;
}

/**
 * Get the value of num
 */ 
public function getNum()
{
return $this->num;
}

/**
 * Set the value of num
 *
 * @return  self
 */ 
public function setNum($num)
{
$this->num = $num;

return $this;
}

/**
 * Get the value of lateralite
 */ 
public function getLateralite()
{
return $this->lateralite;
}

/**
 * Set the value of lateralite
 *
 * @return  self
 */ 
public function setLateralite($lateralite)
{
$this->lateralite = $lateralite;

return $this;
}
}

?>