<?php


/**
 * Function qui permet d'établir la co a la BDD 
 * retourne la connexion
 */
function etablirCo()
{
    $urlSGBD="localhost";
    $nomBDD="stagiaire_cachan"; // le nom de la BDD
    $loginBDD="root";
    $mdpBDD="";// le mdp est root si on utilise Mamp
    try{
    $connex = new PDO("mysql:host=$urlSGBD;dbname=$nomBDD", "$loginBDD", "$mdpBDD", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $connex->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $error) {
        echo "Il y a un problème de co a la BDD verifier que la bdd est presente et les lignes 7 à 10 du fichier bdd.php<br>";
        echo $error->getMessage();
    }
    
    return $connex;
}

/**
 * Permet de recuperer les infos des stagiaires(tous)
 */
function getAllStagiaire(){

        try {
            require("./model/classes/stagiaire.class.php");
            $bdd=etablirCo();
            $sql =$bdd->prepare("SELECT * FROM stagiaire ORDER BY idStagiaire");
            $sql->execute();
            $resultat = ($sql->fetchAll(PDO::FETCH_CLASS,"Stagiaire"));
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
    function addStagiaire($nom,$prenom,$email){

        try {
            $bdd=etablirCo();
            $sql =$bdd->prepare("INSERT INTO stagiaire (nom,prenom,email) VALUES ('$nom','$prenom','$email')");
            $sql->execute();
            // $resultat = ($sql->fetchAll());
            // return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    function getStagiaireById($id)
    {
        try {
            $connex=etablirCo();
            //ATTENTION NE PAS AJOUTER L'ID COMME CELA DANS UNE VRAI APP
            //CAR ON NE PROTEGE PAS CONTRE LES INJECTIONS SQL
            $sql =$connex->prepare("SELECT * FROM stagiaire WHERE idStagiaire=$id ");
            $sql->execute();
            $resultat = ($sql->fetchAll());
            return $resultat[0];

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    function getEvaluationByIdStagiaire($idStagiaire){

        try {
            $connex=etablirCo();
            $sql =$connex->prepare("SELECT * FROM evaluation NATURAL JOIN matiere WHERE idStagiaire=$idStagiaire ORDER BY nomMatiere");
            $sql->execute();
            $resultat = ($sql->fetchAll());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }



?>