<?php
class Fruit {
    private $couleur;
    private $nom;
    private $avoirPepins;
    private $avoirNoyau;

    public function __construct($uneCouleur,$unNom,$unPepins,$unNoyau)
    {
        $this->couleur=$uneCouleur;
        $this->nom=$unNom;
        $this->avoirNoyau=$unNoyau;
        $this->avoirPepins=$unPepins;
    }
    public function quiSuisJe()
    {
        return "Je suis un/une ".$this->nom." J'ai comme couleur ".$this->couleur;
    }



    /**
     * Get the value of couleur
     */ 
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set the value of couleur
     *
     * @return  self
     */ 
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of avoirPepins
     */ 
    public function getAvoirPepins()
    {
        return $this->avoirPepins;
    }

    /**
     * Set the value of avoirPepins
     *
     * @return  self
     */ 
    public function setAvoirPepins($avoirPepins)
    {
        $this->avoirPepins = $avoirPepins;

        return $this;
    }

    /**
     * Get the value of avoirNoyau
     */ 
    public function getAvoirNoyau()
    {
        return $this->avoirNoyau;
    }

    /**
     * Set the value of avoirNoyau
     *
     * @return  self
     */ 
    public function setAvoirNoyau($avoirNoyau)
    {
        $this->avoirNoyau = $avoirNoyau;

        return $this;
    }
}

$objet1=new Fruit("Orange","Clémantine",true,false);
var_dump($objet1);
echo $objet1->getCouleur();//"Orange"
$objet1->setNom("Banane");
var_dump($objet1);
echo $objet1->quiSuisJe();
// $objet1->couleur="Orange";
// var_dump($objet1);
?>