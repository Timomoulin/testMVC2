<?php
class Rencontre{
private $equipe1;
private $equipe2;
private $score;
private $lieux;
private $date;


/**
 * Get the value of equipe1
 */ 
public function getEquipe1()
{
return $this->equipe1;
}

/**
 * Set the value of equipe1
 *
 * @return  self
 */ 
public function setEquipe1($equipe1)
{
$this->equipe1 = $equipe1;

return $this;
}

/**
 * Get the value of equipe2
 */ 
public function getEquipe2()
{
return $this->equipe2;
}

/**
 * Set the value of equipe2
 *
 * @return  self
 */ 
public function setEquipe2($equipe2)
{
$this->equipe2 = $equipe2;

return $this;
}

/**
 * Get the value of score
 */ 
public function getScore()
{
return $this->score;
}

/**
 * Set the value of score
 *
 * @return  self
 */ 
public function setScore($score)
{
$this->score = $score;

return $this;
}

/**
 * Get the value of lieux
 */ 
public function getLieux()
{
return $this->lieux;
}

/**
 * Set the value of lieux
 *
 * @return  self
 */ 
public function setLieux($lieux)
{
$this->lieux = $lieux;

return $this;
}

/**
 * Get the value of date
 */ 
public function getDate()
{
return $this->date;
}

/**
 * Set the value of date
 *
 * @return  self
 */ 
public function setDate($date)
{
$this->date = $date;

return $this;
}
}
?>