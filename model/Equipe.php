<?php 
class Equipe{
    private $titulaires;
    private $remplaants;
    private $formation;

    /**
     * Get the value of titulaires
     */ 
    public function getTitulaires()
    {
        return $this->titulaires;
    }

    /**
     * Set the value of titulaires
     *
     * @return  self
     */ 
    public function setTitulaires($titulaires)
    {
        $this->titulaires = $titulaires;

        return $this;
    }

    /**
     * Get the value of remplaants
     */ 
    public function getRemplaants()
    {
        return $this->remplaants;
    }

    /**
     * Set the value of remplaants
     *
     * @return  self
     */ 
    public function setRemplaants($remplaants)
    {
        $this->remplaants = $remplaants;

        return $this;
    }

    /**
     * Get the value of formation
     */ 
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * Set the value of formation
     *
     * @return  self
     */ 
    public function setFormation($formation)
    {
        $this->formation = $formation;

        return $this;
    }
}
?>