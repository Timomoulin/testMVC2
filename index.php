<?php
session_start();
if(!isset($_GET['path']))
{
    $path="main";
}
else{
    $path=filter_var($_GET['path'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}

    switch ($path){
        case "main":
        require('controller/controller.php');
        break;

        case "admin":
            if(isset($_SESSION['role']) )
            {
                if($_SESSION['role']=="admin")
                {
                    require('controller/adminController.php');
                }
                
            }
            else{
                require('view/404.php');
            }
        
        break;

        default :
        include("view/404.php");
    }



?>