<?php
$title="Login";

ob_start();?>
<div class="container d-flex flex-column justify-content-center">
<h1>Se connecter</h1>
<?php
if(isset($_SESSION['error']))
{
    echo ("<div class='text-danger'>".$_SESSION['error']."</div>");
}
?>
<form method="post" action="./?path=main&action=handleCo" class=" d-flex flex-column align-items-center">
    <div>
        <label for="inputMail">Login :</label>
        <input required minlength="6" id="inputMail" type="mail" name="email" class="form-control">
    </div>
    <div>
        <label for="inputMdp">Mdp :</label>
        <input required minlength="6" id="inputMdp" name="mdp" type="password" class="form-control">
    </div>
    <button class="btn btn-secondary m-2">Envoyer</button>
</form>
</div>
<?php $content=ob_get_clean();
require("template.php");
