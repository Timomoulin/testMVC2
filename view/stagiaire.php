<?php
$title="Stagiaire";

ob_start();?>
<div class="container d-flex flex-column justify-content-center">
<h1>stagiaire</h1>
<h2>Nom : <?php echo $leStagiaire->getNom(); ?></h2>
<h2>Prenom : <?php echo $leStagiaire->getPrenom(); ?></h2>
<h2>Email : <?php echo $leStagiaire->getEmail(); ?></h2>
<table class="table">
    <thead>
        <tr>
            <td>Date</td>
            <td>Note</td>
            <td>Matiere</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
    <?php 
   
    foreach($lesEvaluations as $uneEvaluation)
    {
        echo("<tr>");
        echo("<td>".$uneEvaluation->getDateEvaluation()."</td>");
        echo("<td>".$uneEvaluation->getNote()."</td>");
        $matiere=$matiereManager->getMatiereById($uneEvaluation->getIdMatiere());
        $nomMatiere=$matiere->getNomMatiere();
        echo("<td>".$nomMatiere."</td>");
      
        echo("<td>");
        echo("<a class='btn btn-info' href='./'>Modifier</a>");
        echo("<a class='btn btn-info' href='./'>Supprimer</a>");
        echo("</td>");
        echo("</tr>");
    }
    ?>
    </tbody>
</table>
</div>
<?php $content=ob_get_clean();
require("template.php");
