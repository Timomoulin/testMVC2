<?php $title = 'Home'; ?>

<?php ob_start(); ?>
<div class="container">
<table class="table">
    <thead>
        <tr>
            <th>Nom :</th>
            <th>Prenom :</th>
            <th>Email : </th>
            <th>Actions :</th>
        </tr>
    </thead>
    <tbody>
    <?php 
    foreach($lesStagiaires as $unStagiaire)
    {
        echo("<tr>");
        echo("<td>".$unStagiaire->getNom()."</td>");
        echo("<td>".$unStagiaire->getPrenom()."</td>");
        echo("<td>".$unStagiaire->getEmail()."</td>");
        echo("<td>");
        echo("<a href='' class='btn btn-info'>Modifier</a>");
        echo("<a href='/?path=main&action=stagiaire&num=".$unStagiaire->getIdStagiaire()."' class='btn btn-info'>Consulter</a>");
        echo("</td>");
        echo("</tr>");
    }
    ?>
    </tbody>
</table>

    <h1 class="d-flex justify-content-center">Moondeers</h1>
    <div class="row d-flex justify-content-around align-items-center m-2 ">
        <div class="col-lg-5 col-12">
            <img class="img-fluid" src="../public/images/barista.jpg" alt="machine a café">
        </div>
        <div class="col-lg-5 col-12">
            <h3>De délicieuses boissons préparées à la main et de savoureux produits sucrés et salés.</h2>
                <p>Il est vrai qu’une tasse de café parfaite et un délicieux petit encas peuvent rendre votre journée
                    meilleure. Venez découvrir nos « mariages parfaits » dans nos salons de café. Nous nous assurons que
                    chaque produit que vous choisissez soit de la plus grande qualité. Après tout, la vie ne
                    devrait-elle pas être ainsi ?</p>
        </div>
    </div>
    <div class="row d-flex justify-content-around align-items-center m-2 ">
        <div class="col-lg-5 col-12">
        <h3>Un été haut en couleurs ! </h3>
        <p> Découvrez notre gamme de Smoothies et commencez l'été sous le signe de la fraîcheur !</p> 
        </div>
       
        
        <div class="col-lg-5 col-12">
        <img class="img-fluid" src="../public/images/smoothies.png" alt="">
    </div>

</div>
<script>
async function fetchJSON(url)
{
    let response= await fetch(url);
    let data= await response.json();
    console.log(await data);
    return data;
}

fetchJSON("https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY");

</script>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>