<?php
$title="Forumulaire de modification d'un Stagiaire";
ob_start();
?>
<div class="container">
<form action="./?path=admin&action=traitementFormModif&amp" method="POST">
    <input type="hidden" name="id" required value=<?php echo $leStagiaire->getIdStagiaire(); ?>>
    <div class="my-2">
    <label for="inputNom">Nom *</label>
    <input value=<?php echo $leStagiaire->getNom(); ?> id="inputNom" name="nom" minlength="2" required type="text" class="form-control">
    </div>

    <div class="my-2">
    <label for="inputPrenom">Prenom *</label>
    <input value=<?php echo $leStagiaire->getPrenom(); ?> id="inputPrenom" name="prenom" required minlength="2" type="text" class="form-control">
    </div>

    <div class="my-2">
    <label for="inputEmail">Email *</label>
    <input value=<?php echo $leStagiaire->getEmail();?> id="inputEmail" name="email" required type="email" class="form-control">
    </div>

    <button class="btn btn-info">Modifier</button>
</form>
</div>
<script src="./public/js/verifForm.js"></script>
<?php $content=ob_get_clean();
require("template.php");
?>