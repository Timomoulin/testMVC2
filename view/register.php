<?php
$title="Formulaire inscription";
ob_start();
?>
<div class="container">

    <form action="index.php/?path=main&action=traitementInscription" method="post" class="d-flex justify-content-center">
        <section class="col-lg-6 col-md-8">
            <h1>Formulaire d'inscription</h1>
            <div class="my-1"><label for="inputPrenom">Prenom* :</label>
                <input minlength="2" required name="prenom" id="inputPrenom" type="text" class="form-control "></div>
            <div class="my-1"><label for="inputNom">Nom* :</label>
                <input minlength="2" required name="nom" type="text" id="inputNom" class="form-control "></div>
            <div class="my-1"><label for="inputEmail">Email* :</label>
                <input required name="email" type="email" id="inputEmail" class="form-control"></div>
            <div class="my-1"><label for="inputMdp1">Mdp* :</label>
                <input pattern="^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,15})$" minlength="8" required name="mdp1" type="password" id="inputMdp1" class="form-control"></div>
            <div class="my-1"><label for="inputMdp2">Confirmation Mdp* :</label>
                <input minlength="8" required name="mdp2" type="password" id="inputMdp2" class="form-control"></div>
            <div class="my-2 d-flex justify-content-center"><button class="px-3 btn btn-success ">Envoyer</button></div>
        </section>
    </form>
    <script>
    let divs=document.querySelectorAll("form input,form select,form texarea")
    for(let input of divs){
        input.addEventListener("blur",function(event)
    {
        if(event.target.checkValidity())
        {
            event.target.classList.add("is-valid");
            event.target.classList.remove("is-invalid");
        }
        else{
            event.target.classList.add("is-invalid");
            event.target.classList.remove("is-valid");
        }
    })
    }
    </script>
</div>
<?php $content=ob_get_clean();
require("template.php");?>